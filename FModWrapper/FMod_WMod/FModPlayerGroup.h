#pragma once

#include "WMod\PlayerGroup.h"

namespace FMOD
{
	class ChannelGroup;
}

namespace wm
{
	class FModPlayerGroup : public PlayerGroup
	{

	private:

		FMOD::ChannelGroup *m_ChannelGroup;

	public:

		FModPlayerGroup(FMOD::ChannelGroup *i_ChannelGroup);

		virtual void AddPlayer(Player *i_ToAdd) override;
		virtual void RemovePlayer(Player *i_ToRemove) override;

		virtual void Play() override;
		virtual void Pause() override;
		virtual void Stop() override;

		virtual void SetVolume(float i_Volume) override;

		FMOD::ChannelGroup* GetFmodChannelGroup();

		~FModPlayerGroup();
	};
}
