#pragma once

namespace wm
{
	class Player
	{

	public:

		virtual void Play() = 0;
		virtual void Pause() = 0;
		virtual void Stop() = 0;

		virtual void SetVolume(float i_Volume) = 0;
		virtual void SetPan(float i_Pan) = 0;
		virtual void SetLoop(bool i_Mode) = 0;

		virtual float GetPan() = 0;
		virtual float GetVolume() = 0;

		virtual ~Player() = 0
		{

		}
	};
}