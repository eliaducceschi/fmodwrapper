#include "FModSystem.h"
#include "FModSound.h"
#include "FModPlayer.h"
#include "FModPlayerGroup.h"

#include "fmod.hpp"

#include <assert.h>

namespace wm
{
	FModSystem::FModSystem() : m_System(nullptr)
	{
	}

	void FModSystem::Initialize()
	{
		FMOD_RESULT result = FMOD::System_Create(&m_System);

		if (result == FMOD_OK)
		{
			unsigned int version = 0;
			m_System->getVersion(&version);
			if (version >= FMOD_VERSION)
			{
				result = m_System->init(32, FMOD_INIT_NORMAL, nullptr);
				if (result == FMOD_OK)
				{
					return;
				}
			}
		}

		m_System = nullptr;
	}

	Sound* FModSystem::LoadSound(const std::string &i_Path)
	{
		FMOD::Sound *fmodSound;
		FMOD_RESULT result = m_System->createSound(i_Path.c_str(), FMOD_DEFAULT, 0, &fmodSound);
		if (result == FMOD_OK)
		{
			Sound *toReturn = new FModSound(fmodSound);
			return toReturn;
		}

		return nullptr;
	}

	void FModSystem::UnloadSound(Sound *i_ToUnload)
	{
		assert(("Sound* can't be down-casted to FModSound*", dynamic_cast<FModSound*>(i_ToUnload) != nullptr));

		FModSound *fmodSound = static_cast<FModSound*>(i_ToUnload);
		fmodSound->GetFmodSound()->release();
		delete i_ToUnload;
	}

	Player* FModSystem::CreatePlayer(Sound *i_Sound)
	{
		assert(("Sound* can't be down-casted to FModSound*", dynamic_cast<FModSound*>(i_Sound) != nullptr));

		FModSound *fmodSound = static_cast<FModSound*>(i_Sound);

		FMOD::Channel *fmodChannel;
		FMOD_RESULT result = m_System->playSound((fmodSound->GetFmodSound()), 0, true, &fmodChannel);
		if (result == FMOD_OK)
		{
			Player *toReturn = new FModPlayer(fmodChannel);
			return toReturn;
		}

		return nullptr;
	}

	void FModSystem::DestroyPlayer(Player *i_ToDestroy)
	{
		delete i_ToDestroy;
	}

	PlayerGroup* FModSystem::CreatePlayerGroup(const std::string &i_GroupName)
	{
		FMOD::ChannelGroup *fmodChannelGroup;
		FMOD_RESULT result = m_System->createChannelGroup(i_GroupName.c_str(), &fmodChannelGroup);
		if (result == FMOD_OK)
		{
			PlayerGroup *toReturn = new FModPlayerGroup(fmodChannelGroup);
			return toReturn;
		}

		return nullptr;
	}

	void FModSystem::DestroyPlayerGroup(PlayerGroup *i_ToDestroy)
	{
		assert(("PlayerGroup* can't be down-casted to FModPlayerGroup*", dynamic_cast<FModPlayerGroup*>(i_ToDestroy) != nullptr));

		FModPlayerGroup *fmodPlayerGroup = static_cast<FModPlayerGroup*>(i_ToDestroy);
		fmodPlayerGroup->GetFmodChannelGroup()->release();
		delete i_ToDestroy;
	}

	bool FModSystem::IsInitialized()
	{
		return m_System != nullptr;
	}

	FModSystem::~FModSystem()
	{
		m_System->close();
		m_System->release();
	}
}