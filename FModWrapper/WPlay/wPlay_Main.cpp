#pragma once

#include "wPlay_Main.h"
#include "utility.h"
#include "wPlay_Play.h"
#include "wPlay_ChannelGroup.h"

#include <algorithm>
#include <iostream>
#include <exception>

void displayMainMenu();

void runMainMenuOption(const int i_Choice);

void wPlay_MainMenu()
{
	int choice = 0;

	do
	{
		displayMainMenu();

		choice = getNumber(1, 3);

		std::cout << std::endl;

		runMainMenuOption(choice);

	}
	while (choice != 3);
}

void runMainMenuOption(const int i_Choice)
{
	if (i_Choice == 1)
	{
		wPlay_PlayMenu();
	}
	else if (i_Choice == 2)
	{
		wPlay_ChannelGroupDemo();
	}
}

void displayMainMenu()
{
	std::cout << "WPLAY\n" << std::endl;
	std::cout << "1) Play Sound" << std::endl;
	std::cout << "2) Channel Group Demo" << std::endl;
	std::cout << "3) Exit\n" << std::endl;
}