#include "wPlay_Play.h"
#include "utility.h"

#include "WMod\System.h"
#include "WMod\Sound.h"
#include "WMod\Player.h"

#include "FMod_WMod\FModSystem.h"

#include <iostream>
#include <string>

wm::System *wPlaySystem = nullptr;
wm::Sound *wPlaySound = nullptr;
wm::Player *wPlayPlayer = nullptr;

wm::Sound* AskFile();
void startPlayMenu();
void displayPlayMenu();
void runPlayMenuOption(const int i_Choice);

void wPlay_PlayMenu()
{
	wPlaySystem = new wm::FModSystem();

	wPlaySystem->Initialize();
	if (wPlaySystem->IsInitialized())
	{
		wPlaySound = AskFile();
		if (wPlaySound != nullptr)
		{
			wPlayPlayer = wPlaySystem->CreatePlayer(wPlaySound);
			if (wPlayPlayer != nullptr)
			{
				std::cout <<  std::endl;
				startPlayMenu();
			}
			else
			{
				std::cout << "\nThe player could not be created!\n" << std::endl;
			}
		}
		else
		{
			std::cout << "\nThe file could not be loaded!\n" << std::endl;
		}
	}
	else
	{
		std::cout << "FMod wModSystem error!\n" << std::endl;
	}

	delete wPlaySystem;

	wPlayPlayer = nullptr;
	wPlaySound = nullptr;
	wPlaySystem = nullptr;
}

void runPlayMenuOption(const int i_Choice)
{
	if (i_Choice == 1)
	{
		wPlayPlayer->Play();

		std::cout << "Playing\n" << std::endl;
	}
	else if (i_Choice == 2)
	{
		wPlayPlayer->Pause();

		std::cout << "Paused\n" << std::endl;
	}
	else if (i_Choice == 3)
	{
		wPlayPlayer->Stop();

		std::cout << "Stopped\n" << std::endl;
	}
	else if (i_Choice == 4)
	{
		float volume = wPlayPlayer->GetVolume();
		volume += 0.2f;
		wPlayPlayer->SetVolume(volume);

		std::cout << "Current Volume: " << RoundToDecimal(wPlayPlayer->GetVolume(), 1) << "\n" << std::endl;
	}
	else if (i_Choice == 5)
	{
		float volume = wPlayPlayer->GetVolume();
		volume -= 0.2f;
		wPlayPlayer->SetVolume(volume);

		std::cout << "Current Volume: " << RoundToDecimal(wPlayPlayer->GetVolume(), 1) << "\n" <<  std::endl;
	}
	else if (i_Choice == 6)
	{
		float pan = wPlayPlayer->GetPan();
		pan += 0.2f;
		wPlayPlayer->SetPan(pan);

		std::cout << "Current Pan: " << RoundToDecimal(wPlayPlayer->GetPan(), 1) << "\n" << std::endl;
	}
	else if (i_Choice == 7)
	{
		float pan = wPlayPlayer->GetPan();
		pan -= 0.2f;
		wPlayPlayer->SetPan(pan);

		std::cout << "Current Pan: " << RoundToDecimal(wPlayPlayer->GetPan(), 1) << "\n" << std::endl;

	}
}

void startPlayMenu()
{
	int choice = 0;

	do
	{
		displayPlayMenu();

		choice = getNumber(1, 8);

		std::cout << std::endl;

		runPlayMenuOption(choice);

	}
	while (choice != 8);

	wPlaySystem->DestroyPlayer(wPlayPlayer);
	wPlaySystem->UnloadSound(wPlaySound);

}

void displayPlayMenu()
{
	std::cout << "1) Play" << std::endl;
	std::cout << "2) Pause" << std::endl;
	std::cout << "3) Stop" << std::endl;
	std::cout << "4) Volume +" << std::endl;
	std::cout << "5) Volume -" << std::endl;
	std::cout << "6) Pan Right" << std::endl;
	std::cout << "7) Pan Left" << std::endl;
	std::cout << "8) Back\n" << std::endl;
}

wm::Sound* AskFile()
{
	std::cout << "Enter the name of a playable file into the current folder:\n" << std::endl;
	std::string filePath;
	std::cin >> filePath;

	return wPlaySystem->LoadSound("./" + filePath);
}
