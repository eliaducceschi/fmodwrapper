#pragma once

#include "WMod\Sound.h"

namespace FMOD
{
	class Sound;
}

namespace wm
{
	class FModSound : public Sound
	{

	private:

		FMOD::Sound *m_Sound;

	public:

		FModSound(FMOD::Sound *i_Sound);

		FMOD::Sound* GetFmodSound();

		~FModSound();


	};
}