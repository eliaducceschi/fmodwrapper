
#include "utility.h"

#include <iostream>
#include <string>
#include <chrono>
#include <thread>

int getNumber(int i_Min, int i_Max)
{
	bool error = false;
	int number = 0;

	do
	{
		error = false;

		std::string input;
		std::cin >> input;

		try
		{
			number = std::stoi(input);
			if (number < i_Min || number > i_Max)
			{
				error = true;
			}
		}
		catch (...)
		{
			error = true;
		}

		if (error)
		{
			std::cout << "\nEnter a number between " << i_Min << " and " << i_Max << "!\n" << std::endl;
		}
	}
	while (error);

	return number;
}

void printAndWait(const std::string &i_ToPrint, int i_Seconds)
{
	std::cout << i_ToPrint << std::endl;
	std::this_thread::sleep_for(std::chrono::milliseconds(i_Seconds * 1000));
}

float RoundToDecimal(float toRound, int maxDecimals)
{
	float divider = std::powf(10.0f, static_cast<float>(maxDecimals));

	float rounded = std::round(toRound * divider);
	rounded = rounded / divider;

	if (std::abs(rounded) < 1.0f / divider)
	{
		rounded = 0;
	}

	return rounded;
}