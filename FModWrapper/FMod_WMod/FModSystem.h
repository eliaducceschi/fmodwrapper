#pragma once

#include "WMod\System.h"

namespace FMOD
{
	class System;
}

namespace wm
{
	class FModSystem : public System
	{

	private:

		FMOD::System *m_System;

	public:

		FModSystem();

		virtual void Initialize() override;
		virtual bool IsInitialized() override;

		virtual Sound* LoadSound(const std::string &i_Path) override;
		virtual void UnloadSound(Sound *i_ToUnload) override;

		virtual Player* CreatePlayer(Sound* i_Sound) override;
		virtual void DestroyPlayer(Player* i_ToDestroy) override;

		virtual PlayerGroup* CreatePlayerGroup(const std::string &i_GroupName) override;
		virtual void DestroyPlayerGroup(PlayerGroup* i_ToDestroy) override;

		~FModSystem();

	};
}