#pragma once

#include "WMod\Player.h"

namespace FMOD
{
	class Channel;
}

namespace wm
{
	class FModPlayer : public Player
	{

	private:

		FMOD::Channel *m_Channel;

		float m_Pan;

	public:

		FModPlayer(FMOD::Channel *i_Channel);

		virtual void Play() override;
		virtual void Pause() override;
		virtual void Stop() override;

		virtual void SetVolume(float i_Volume) override;
		virtual void SetPan(float i_Pan) override;
		virtual void SetLoop(bool i_Mode) override;

		virtual float GetVolume() override;
		virtual float GetPan() override;

		FMOD::Channel* GetFmodChannel();

		~FModPlayer();
	};
}
