#pragma once

#include <string>

namespace wm
{
	class Sound;
	class Player;
	class PlayerGroup;

	class System
	{

	public:

		virtual void Initialize() = 0;
		virtual bool IsInitialized() = 0;

		virtual Sound* LoadSound(const std::string &i_Path) =0;
		virtual void UnloadSound(Sound *i_ToUnload) =0;

		virtual Player* CreatePlayer(Sound* i_Sound) =0;
		virtual void DestroyPlayer(Player* i_ToDestroy) = 0;

		virtual PlayerGroup* CreatePlayerGroup(const std::string &i_GroupName) = 0;
		virtual void DestroyPlayerGroup(PlayerGroup* i_ToDestroy) = 0;

		virtual ~System() = 0
		{

		}

	};
}