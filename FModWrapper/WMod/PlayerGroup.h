#pragma once

namespace wm
{
	class Player;

	class PlayerGroup
	{

	public:

		virtual void AddPlayer(Player *i_ToAdd) = 0;
		virtual void RemovePlayer(Player *i_ToRemove) = 0;

		virtual void Play() = 0;
		virtual void Pause() = 0;
		virtual void Stop() = 0;

		virtual void SetVolume(float i_Volume) = 0;

		virtual ~PlayerGroup() = 0
		{

		}
	};
}