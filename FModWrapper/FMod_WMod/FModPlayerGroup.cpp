#include "FModPlayerGroup.h"
#include "FModPlayer.h"

#include "fmod.hpp"

#include <assert.h>

namespace wm
{
	FModPlayerGroup::FModPlayerGroup(FMOD::ChannelGroup *i_ChannelGroup) : m_ChannelGroup(i_ChannelGroup)
	{
	}

	void FModPlayerGroup::AddPlayer(Player *i_ToAdd)
	{
		assert(("Player* can't be down-casted to FModPlayer*", dynamic_cast<FModPlayer*>(i_ToAdd) != nullptr));

		FModPlayer *fmodPlayer = static_cast<FModPlayer*>(i_ToAdd);
		FMOD::Channel *channelToAdd = fmodPlayer->GetFmodChannel();
		channelToAdd->setChannelGroup(m_ChannelGroup);
	}

	void FModPlayerGroup::RemovePlayer(Player *i_ToRemove)
	{
		assert(("Player* can't be down-casted to FModPlayer*", dynamic_cast<FModPlayer*>(i_ToRemove) != nullptr));

		FModPlayer *fmodPlayer = static_cast<FModPlayer*>(i_ToRemove);
		FMOD::Channel *channelToRemove = fmodPlayer->GetFmodChannel();

		FMOD::ChannelGroup *currentChannelGroup;
		channelToRemove->getChannelGroup(&currentChannelGroup);
		if (m_ChannelGroup == currentChannelGroup)
		{
			channelToRemove->setChannelGroup(nullptr);
		}
	}

	void FModPlayerGroup::Play()
	{
		m_ChannelGroup->setPaused(false);
	}

	void FModPlayerGroup::Pause()
	{
		m_ChannelGroup->setPaused(true);
	}

	void FModPlayerGroup::Stop()
	{
		m_ChannelGroup->setPaused(true);

		int groupSize;
		m_ChannelGroup->getNumChannels(&groupSize);

		for (int channelIndex = 0; channelIndex < groupSize; ++channelIndex)
		{
			FMOD::Channel *channelToReset;
			m_ChannelGroup->getChannel(channelIndex, &channelToReset);
			channelToReset->setPosition(0, FMOD_TIMEUNIT_MS);
		}
	}

	void FModPlayerGroup::SetVolume(float i_Volume)
	{
		if (i_Volume < 0)
		{
			i_Volume = 0;
		}

		if (i_Volume > 1.0f)
		{
			i_Volume = 1.0f;
		}

		m_ChannelGroup->setVolume(i_Volume);
	}

	FMOD::ChannelGroup * FModPlayerGroup::GetFmodChannelGroup()
	{
		return m_ChannelGroup;
	}

	FModPlayerGroup::~FModPlayerGroup()
	{
	}
}
