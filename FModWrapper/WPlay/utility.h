#pragma once

#include <string>

int getNumber(int i_Min, int i_Max);

void printAndWait(const std::string &i_ToPrint, int i_Seconds);

float RoundToDecimal(float toRound, int maxDecimals);