#include "wPlay_ChannelGroup.h"
#include "utility.h"

#include "WMod\System.h"
#include "WMod\Sound.h"
#include "WMod\Player.h"
#include "WMod\PlayerGroup.h"

#include "FMod_WMod\FModSystem.h"

#include <iostream>
#include <string>

wm::System *wChannelGroupSystem = nullptr;

wm::Sound *wChannelGroupSoundFirst = nullptr;
wm::Sound *wChannelGroupSoundSecond = nullptr;

wm::Player *wChannelGroupPlayerFirst = nullptr;
wm::Player *wChannelGroupPlayerSecond = nullptr;

wm::PlayerGroup *wChannelGroupPlayerGroupA = nullptr;

void startDemo();

void wPlay_ChannelGroupDemo()
{
	std::cout << "Demo setup, please wait...\n" << std::endl;

	wChannelGroupSystem = new wm::FModSystem();

	wChannelGroupSystem->Initialize();
	if (wChannelGroupSystem->IsInitialized())
	{
		wChannelGroupSoundFirst = wChannelGroupSystem->LoadSound("./First.mp3");
		wChannelGroupSoundSecond = wChannelGroupSystem->LoadSound("./Second.mp3");
		if (wChannelGroupSoundFirst != nullptr && wChannelGroupSoundSecond != nullptr)
		{
			wChannelGroupPlayerFirst = wChannelGroupSystem->CreatePlayer(wChannelGroupSoundFirst);
			wChannelGroupPlayerSecond = wChannelGroupSystem->CreatePlayer(wChannelGroupSoundSecond);
			if (wChannelGroupPlayerFirst != nullptr && wChannelGroupPlayerSecond != nullptr)
			{
				wChannelGroupPlayerGroupA = wChannelGroupSystem->CreatePlayerGroup("Group A");
				if (wChannelGroupPlayerGroupA != nullptr)
				{
					printAndWait("Demo setup completed\n", 1);

					startDemo();

					wChannelGroupSystem->DestroyPlayerGroup(wChannelGroupPlayerGroupA);
					wChannelGroupSystem->DestroyPlayer(wChannelGroupPlayerSecond);
					wChannelGroupSystem->DestroyPlayer(wChannelGroupPlayerFirst);
					wChannelGroupSystem->UnloadSound(wChannelGroupSoundSecond);
					wChannelGroupSystem->UnloadSound(wChannelGroupSoundFirst);
				}
				else
				{
					std::cout << "The player groups could not be created!\n" << std::endl;
				}
			}
			else
			{
				std::cout << "The players could not be created!\n" << std::endl;
			}
		}
		else
		{
			std::cout << "Error loading sound files could not be loaded!\n" << std::endl;
		}
	}
	else
	{
		std::cout << "FMod wChannelGroupSystem error!\n" << std::endl;
	}

	delete wChannelGroupSystem;

	wChannelGroupPlayerGroupA = nullptr;
	wChannelGroupPlayerFirst = nullptr;
	wChannelGroupPlayerSecond = nullptr;
	wChannelGroupSoundFirst = nullptr;
	wChannelGroupSoundSecond = nullptr;
	wChannelGroupSystem = nullptr;

}


void startDemo()
{
	wChannelGroupPlayerFirst->SetVolume(0.4f);
	wChannelGroupPlayerFirst->Play();
	printAndWait("Play \"First.mp3\"", 3);

	wChannelGroupPlayerSecond->Play();
	printAndWait("Play \"Second.mp3\"", 3);

	wChannelGroupPlayerGroupA->AddPlayer(wChannelGroupPlayerFirst);
	printAndWait("\nAdd \"First.mp3\" to \"Group A\"", 2);
	wChannelGroupPlayerGroupA->AddPlayer(wChannelGroupPlayerSecond);
	printAndWait("Add \"Second.mp3\" to \"Group A\"", 2);

	wChannelGroupPlayerGroupA->Pause();
	printAndWait("\nPause \"Group A\"", 2);
	wChannelGroupPlayerGroupA->Play();
	printAndWait("Play \"Group A\"", 3);

	wChannelGroupPlayerFirst->SetVolume(0.2f);
	printAndWait("\nLower volume of \"First.mp3\"", 3);
	wChannelGroupPlayerGroupA->SetVolume(0.2f);
	printAndWait("Lower volume of \"Group A\"", 3);
	wChannelGroupPlayerGroupA->SetVolume(0.5f);
	printAndWait("Raise volume of \"Group A\"", 3);

	wChannelGroupPlayerGroupA->Stop();
	printAndWait("\nStop \"Group A\"", 2);
	wChannelGroupPlayerGroupA->Play();
	printAndWait("Play \"Group A\"", 3);
	wChannelGroupPlayerGroupA->Pause();
	printAndWait("Pause \"Group A\"", 2);
	wChannelGroupPlayerGroupA->Play();
	printAndWait("Play \"Group A\"", 3);
	wChannelGroupPlayerGroupA->Stop();
	printAndWait("Stop \"Group A\"", 2);

	printAndWait("\nDemo completed\n", 1);
}
