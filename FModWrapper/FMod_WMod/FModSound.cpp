#include "FModSound.h"

#include "fmod.hpp"

namespace wm
{
	FModSound::FModSound(FMOD::Sound * i_Sound) : m_Sound(i_Sound)
	{
	}

	FMOD::Sound * FModSound::GetFmodSound()
	{
		return m_Sound;
	}

	FModSound::~FModSound()
	{
	}
}