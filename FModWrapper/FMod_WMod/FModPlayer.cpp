#include "FModPlayer.h"

#include "fmod.hpp"

namespace wm
{

	FModPlayer::FModPlayer(FMOD::Channel * i_Channel) : m_Channel(i_Channel), m_Pan(0)
	{
		i_Channel->setPan(m_Pan);
	}

	void FModPlayer::Play()
	{
		m_Channel->setPaused(false);
	}

	void FModPlayer::Pause()
	{
		m_Channel->setPaused(true);
	}

	void FModPlayer::Stop()
	{
		m_Channel->setPaused(true);
		m_Channel->setPosition(0, FMOD_TIMEUNIT_MS);
	}

	void FModPlayer::SetVolume(float i_Volume)
	{
		if (i_Volume < 0)
		{
			i_Volume = 0;
		}

		if (i_Volume > 1.0f)
		{
			i_Volume = 1.0f;
		}

		m_Channel->setVolume(i_Volume);
	}

	void FModPlayer::SetPan(float i_Pan)
	{
		if (i_Pan < -1.0f)
		{
			i_Pan = -1.0f;
		}

		if (i_Pan > 1.0f)
		{
			i_Pan = 1.0f;
		}

		m_Pan = i_Pan;
		m_Channel->setPan(m_Pan);
	}

	void FModPlayer::SetLoop(bool i_Mode)
	{
		if (i_Mode)
		{
			m_Channel->setMode(FMOD_LOOP_NORMAL);
		}
		else
		{
			m_Channel->setMode(FMOD_LOOP_OFF);
		}
	}

	float FModPlayer::GetVolume()
	{
		float volume = 0;
		m_Channel->getVolume(&volume);
		return volume;
	}

	float FModPlayer::GetPan()
	{
		return m_Pan;
	}

	FMOD::Channel * FModPlayer::GetFmodChannel()
	{
		return m_Channel;
	}

	FModPlayer::~FModPlayer()
	{
	}
}
